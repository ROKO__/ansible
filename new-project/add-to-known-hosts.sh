#!/bin/bash

hostname="$1"
port="$2"
path="$3"

if [ -z "$hostname" ] || [ -z "$port" ] || [ -z "$path" ]; then
    exit 1
fi

if ssh-keygen -q -F "$hostname" -f "$path" >/dev/null 2>&1; then
    exit 0
fi

if echo "# Host: '${hostname}:$port'" &&
    ssh-keyscan -T 15 -H -p "${port}" "${hostname}"; then
    exit 0
fi

exit 1
