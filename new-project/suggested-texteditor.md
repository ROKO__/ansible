**_As yaml editor i suggest you to use [Atom Editor](https://atom.io/) with the
following plugins._**

[atom-ide-ui](https://atom.io/packages/atom-ide-ui)
[ide-yaml](https://atom.io/packages/ide-yaml)
