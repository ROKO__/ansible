# A little doc related to ansible playbooks.

## Warning!!!
  **Before aplying any playbook please check it with text editor, suggested one is [Atom](./suggested-texteditor.md).**

### Select hosts/inventories by editing playbook.
- Open the playbook yaml file with some text editor (I suggest you to use Atom) and edit the `hosts` block, then add specific host, for multiple hosts you have to add new line for each starting with hyphen (-). Take a look at the following example.

  ```
  - name: My cool stuff
    hosts:
      - host or IP
      - host or IP
      - host or IP
    ...
      tasks:
        -name: my cool tasks
    ...

### Select hosts/inventories by separating inventory on pieces (Recommended by Ansible)
- Create multiple files with desired names for each inventory inside    them, and run ansible playbook with some of inventories by tipping the  command from the following example.

  **_Lets create sample inventory file. (Possible formats ini and yaml), the following example is with default ini format._**

  ```
  [my-inventory]
  my-host.org
  123.123.123.123
  ```

  **_Note: '-i' parameter is used to invoke inventory file._**

  `ansible-playbook my-playbook.yml -i inventory-name`

  **If everything is correct you'll see similar to the following output.**

  `ansible-playbook ping.yml -i myinventory.ini`

    ```
    PLAY [Test connection to servers] ******************************************************************************************************************************************

    TASK [Ping servers] ********************************************************************************************************************************************************

    ok: [bitaka.internal-mapping.com]
    ok: [watermelon.internal-mapping.com]
    ok: [roi-app-main.internal-mapping.com]

    PLAY RECAP *****************************************************************************************************************************************************************
    bitaka.internal-mapping.com : ok=1    changed=0    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0
    roi-app-main.internal-mapping.com : ok=1    changed=0    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0
    watermelon.internal-mapping.com : ok=1    changed=0    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0

### Select hosts/inventories by using ansible '--limit' command line parameter. (Recommended by me).
  - Example with --limit parameter. Hosts must be comma separated!  Note: you need to add new hosts to main inventory file, otherwise ansible will complain for non existing hosts.

    **If everything is correct you'll see similar to the following output.**

    `ansible-playbook ping.yml -i hosts.ini --limit bitaka.internal-mapping.com,lamarina.internal-mapping.com,massa.internal-mapping.com`

    ```
    PLAY [Test connection to servers] ******************************************************************************************************************************************

    TASK [Ping servers] ********************************************************************************************************************************************************
    ok: [lamarina.internal-mapping.com]
    ok: [bitaka.internal-mapping.com]
    ok: [massa.internal-mapping.com]

    PLAY RECAP *****************************************************************************************************************************************************************
    bitaka.internal-mapping.com : ok=1    changed=0    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0
    lamarina.internal-mapping.com : ok=1    changed=0    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0
    massa.internal-mapping.com : ok=1    changed=0    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0

### Some example cmdlines.
  - **_In most cases it's enough just to execute the playbook without any cmdline parameters._**
  - By passing cmdline parameters, the following example is with the replace-user-public-key.yml and update-pubkey-replace-existing.yml playbooks.
  - `-e` parameter is used for extra options, you can see them in double quotes. Note that this variables could be pretty different, depend on how you've made the playbook.
  - `-i` parameter is used for custom inventory file (usually ini format).
  - `--limit` is already explained bellow.
  - `-v` verbose (nothing to explain here).
  - `-C` dry-run (nothing to explain here).

    `ansible-playbook -e "user_name=joro old_public_key=keys/joro-devops.pub public_key=keys/rmarinchev-devops.pub" replace-user-public-key.yml -i ~/servers.ansible/hosts.ini --limit 192.168.56.237 -v -C`

    `ansible-playbook -e "user_name=joro public_key=keys/joro-devops.pub" replace-all-public-keys.yml -i ~/servers.ansible/hosts.ini --limit 192.168.56.237 -v -C`

  ### **Usually you can execute ad-hoc command instead of a playbook, i'll not go too deep right now, just one example.**

  - `-m` parameter is used to inherit ansible module.

      `ansible all -m shell -a 'uptime'` with sample output.

      ```
      ai-office.internal-mapping.com | CHANGED | rc=0 >>
      16:39:48 up 29 days, 23:48,  2 users,  load average: 0.01, 0.06, 0.08
      192.168.56.237 | CHANGED | rc=0 >>
      13:39:48 up 14 days,  5:24,  2 users,  load average: 0.00, 0.00, 0.00
      kbn-elastic-main-bitaka.internal-mapping.com | CHANGED | rc=0 >>
      13:39:48 up 34 days,  4:12,  1 user,  load average: 0.05, 0.05, 0.01
      kbn-elastic-n1-bitaka.internal-mapping.com | CHANGED | rc=0 >>
      13:39:49 up 34 days,  4:12,  1 user,  load average: 0.72, 0.50, 0.42
      ```

### Optimizations<br>

**_Description_**

  >  Pipelining, if supported by the connection plugin, reduces the number of network operations required to execute a module on the remote server, by executing many Ansible modules without actual file transfer. This can result in a very significant performance improvement when enabled. However this conflicts with privilege escalation (become). For example, when using ‘sudo:’ operations you must first disable ‘requiretty’ in /etc/sudoers on all managed hosts, which is why it is disabled by default. This option is disabled if ANSIBLE_KEEP_REMOTE_FILES is enabled.
  `type boolean true/false`.

  > callback_whitelist enables a summary about execution time of any task inside the playbooks.

  > forks is used to let ansible use more/less cpu power (default 5 too conservative, better to use double of cpu cores)

> retries count means how many times it will retry before going to the next host.

> internal_poll_interval sets the interval (in seconds) of Ansible internal processes polling each other. Lower values improve performance with large playbooks at the expense of extra CPU load. Higher values are more suitable for Ansible usage in automation scenarios, when UI responsiveness is not required but CPU usage might be a concern. Default corresponds to the value hardcoded in Ansible ≤ 2.1. (Default Unset)

  - ansible.cfg

  ```
  [defaults]
  # mod, it will need more cpu power.
  callback_whitelist = profile_tasks
  forks = 20
  internal_poll_interval=0.001
  [ssh_connection]
  retries=3
  pipelining = true
 ```

  - playbook
  _`strategy: free` means parallel tasks execution on all mentioned hosts._

  ```
  hosts: all
  strategy: free
  ```

# **_Warning_**
**It can cause high cpu and ram usage!**
